<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([], function($router){
    $router->group(['namespace' => 'Location'], function($router){
        $router->get('/cities', 'CitiesController@get');
        $router->get('/provinces', 'ProvincesController@get');
        $router->get('/branches', 'BranchesController@get');
    });

   	$router->post('/check-in', 'ContactTraceController@store');
});
