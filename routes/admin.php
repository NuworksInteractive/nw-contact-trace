<?php

Route::get('/', 'LoginController@getRedirectToLoginPage');
Route::get('login', 'LoginController@getLoginPage')->name('admin.get.login');
Route::get('login?error=1', 'LoginController@getLoginPage')->name('admin.get.login.with-error');
Route::post('login', 'LoginController@postAuthenticate')->name('admin.post.login');

Route::group(['middleware' => 'auth.admin'], function($r) {
	$r->get('logout', 'LoginController@getLogOut')->name('admin.get.logout');
	$r->get('dashboard', 'DashboardController@getDashboardPage')->name('admin.get.dashboard');
	$r->get('dashboard/reports-data', 'DashboardController@getData')->name('admin.get.dashboard.data');
	$r->post('dashboard/reports-data/delete', 'DashboardController@postDeleteData')->name('admin.post.dashboard.delete');
	$r->get('dashboard/reports-data/export', 'DashboardController@getDataExport')->name('admin.get.dashboard.data.export');
});