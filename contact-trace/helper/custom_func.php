<?php

if( ! function_exists('kafka_producer'))
{
	function kafka_producer()
	{
		return app(ContactTraceModules\Kafka\Producer::class);
	}
}