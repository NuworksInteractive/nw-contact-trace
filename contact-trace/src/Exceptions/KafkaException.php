<?php

namespace ContactTraceModules\Exceptions;

use ContactTraceModules\Exceptions\ContactTraceException;

class KafkaException extends ContactTraceException
{
    /**
     * Invalid topic
     *
     * @return static
     */
    public static function InvalidTopic()
    {
        return new static("Invalid topic given.", HTTP_RESPONSE_ENTITY_NOT_PROCESS);
    }

    /**
     * No data given
     *
     * @return static
     */
    public static function NoDataGiven()
    {
        return new static("No data given.", HTTP_RESPONSE_ENTITY_NOT_PROCESS);
    }
}