<?php

namespace ContactTraceModules\Exceptions;

use ContactTraceModules\Exceptions\ContactTraceException;

class LoginException extends ContactTraceException
{

    /**
     * Invalid credentials
     *
     * @return static
     */
    public static function invalidCredentials()
    {
        return new static("Invalid account login.", HTTP_RESPONSE_UNAUTHORIZED);
    }

    /**
     * User not found
     *
     * @return static
     */
    public static function accountNotFound()
    {
        return new static("Account not found", HTTP_RESPONSE_NOT_FOUND);
    }  
}