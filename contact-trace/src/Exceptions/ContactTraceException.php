<?php

namespace ContactTraceModules\Exceptions;

use Exception;

abstract class ContactTraceException extends Exception
{

    public function __construct($message, $code = 500, Exception $previous = null)
    {
        \Log::error($message);
        parent::__construct($message, $code, $previous);
    }

}