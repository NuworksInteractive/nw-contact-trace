<?php

namespace ContactTraceModules\Exceptions;

use ContactTraceModules\Exceptions\ContactTraceException;

class UnauthorizedAccessException extends ContactTraceException
{
    public function __construct()
    {
        parent::__construct('Unauthorized Access', HTTP_RESPONSE_UNAUTHORIZED);
    }
}