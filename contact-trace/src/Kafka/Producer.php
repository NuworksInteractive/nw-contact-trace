<?php

namespace ContactTraceModules\Kafka;

use ContactTraceModules\Exceptions\KafkaException;

class Producer {
	/**
	 * List of topics
	 */
	const TOPICS = [
		'check_in',
		'suspect',
		'self_isolate',
		'positive',
	];

	/**
	 * @var array
	 */
	protected $data;

	/**
	 * @var string
	 */
	protected $topic;

	/**
	 * Sends the given data to MoM to process
	 *
	 * @return Exception|null
	 */
	public function send()
	{
		$config = new \RdKafka\Conf();
		$config->set('metadata.broker.list', '192.168.1.10:9092');

		$producer = new \RdKafka\Producer($config);

		$topic = $producer->newTopic($this->getTopic());
		$topic->produce(RD_KAFKA_PARTITION_UA, 0, json_encode($this->getData()));

    	$producer->poll(0);

        $this->flushRetries($producer, $result);

        if (RD_KAFKA_RESP_ERR_NO_ERROR !== $result) {
            throw new \RuntimeException('Was unable to flush, messages might be lost!');
        }
	}

	/**
	 * Flush the producer's retries
	 * 
	 * @param  [type] $producer \RdKafka\Producer
	 * @param  [type] &$result  The flush result
	 * 
	 * @return mixed
	 */
	private function flushRetries($producer, &$result)
	{
		for ($flushRetries = 0; $flushRetries < 10; $flushRetries++) {
            $result = $producer->flush(10000);
            if (RD_KAFKA_RESP_ERR_NO_ERROR === $result) {
                break;
            }
        }
	}

	/**
	 * Set the data attribute
	 * 
	 * @param array $data
	 * 
	 * @return self
	 */
	public function set_data($data = [])
	{
		if( ! $data) {
			throw KafkaException::NoDataGiven();
		}

		$this->data = $data;

		return $this;
	}

	/**
	 * Set the topic attribute
	 * 
	 * @param string $topic
	 *
	 * @return self
	 */
	public function set_topic($topic = null)
	{
		if( ! in_array($topic, self::TOPICS)) {
			throw KafkaException::InvalidTopic();
		}

		$this->topic = $topic;

		return $this;
	}

	/**
	 * Get the $data attribute
	 * 
	 * @return array
	 */
	private function getData()
	{
		return $this->data;
	}

	/**
	 * Get the $topic attribute
	 * 
	 * @return string
	 */
	private function getTopic()
	{
		return $this->topic;
	}
}