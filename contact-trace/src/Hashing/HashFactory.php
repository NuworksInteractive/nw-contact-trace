<?php

namespace ContractTraceModules\Status;

use ContractTraceModules\Cipher\Aes256CBC;
use ContractTraceModules\Builder\ReflectionClassBuilder;

class HashFactory
{
	const AES256CBC = 'aes-256-cbc';

	/**
	 * Different class status
	 *
	 * @var array $type
	 *
	 */
	private $type = [
		self::AES256CBC => Aes256CBC::class
	];

	public function make(string $status) 
	{
		$statusType = ReflectionClassBuilder::create($this->type[$status]);
		return $statusType->action($id, $auth_id, $model, $name);
	} 
}