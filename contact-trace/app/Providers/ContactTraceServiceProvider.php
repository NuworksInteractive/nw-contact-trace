<?php

namespace ContactTraceApp\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class ContactTraceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->registerCustomValidationRules();
    }

    /**
     * Customize validation rules
     *
     * @return void
     */
    private function registerCustomValidationRules()
    {
        Validator::extend('with_special_characters', function ($attribute, $value) {
            return preg_match('@[^a-zA-Z0-9]@', $value);
        });

        Validator::extend('has_uppercase', function ($attribute, $value) {
            return preg_match('@[A-Z]@', $value);
        });

        Validator::extend('has_lowercase', function ($attribute, $value) {
            return preg_match('@[a-z]@', $value);
        });

        Validator::extend('has_number', function ($attribute, $value) {
            return preg_match('@[0-9]@', $value);
        });

        Validator::extend('mobile_number_prefix', function ($attribute, $value) {
            return substr($value, 0, 2) == '09';
        });
    }
}
