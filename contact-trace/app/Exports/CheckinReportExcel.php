<?php

namespace ContactTraceApp\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class CheckinReportExcel implements FromCollection
{
    use Exportable;

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        $heading = [
            'Timestamp',
            'Firstname',
            'Lastname',
            'Status',
            'City',
            'Province',
        ];

        $data = [];

        if($this->data) {
            foreach($this->data as $i => $record) {
                $data[$i] = [
                    $record[0],
                    $record[1],
                    $record[2],
                    $record[3],
                    $record[4],
                    $record[5],
                ];
            }
        }

        return new Collection([$heading, $data]);
    }
}
