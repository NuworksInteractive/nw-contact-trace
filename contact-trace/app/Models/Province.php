<?php 

namespace ContactTraceApp\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';

  	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'province_name',
		'country_name'
    ];

    public function cities()
    {
        return $this->hasMany(\ContactTraceApp\Models\City::class, 'province_id');
    }
}
