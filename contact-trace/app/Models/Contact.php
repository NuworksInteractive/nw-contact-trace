<?php 

namespace ContactTraceApp\Models;

use Illuminate\Database\Eloquent\Model;
use ContactTraceApp\Models\Traits\Encryptable;

class Contact extends Model
{
    use Encryptable;

    protected $table = 'contacts';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'contact_id_no',
        'firstname',
        'lastname',
        'email',
        'mobile_no',
        'current_address',
        'address_province_id',
        'address_city_id'
    ];

    /**
     * The attributes that should be encrypted
     *
     * @var array
     */
    protected $encryptable = [
        'contact_id_no',
        'firstname',
        'lastname',
        'email',
        'mobile_no',
        'current_address',
        'address_province_id',
        'address_city_id'
    ];

    public function checkin()
    {
        return $this->hasOne(\ContactTraceApp\Models\CheckIn::class, 'contact_id');
    }

    public function questionnaire_answers()
    {
        return $this->hasMany(\ContactTraceApp\Models\QuestionnaireAnswer::class, 'contact_id', 'id');
    }
}
