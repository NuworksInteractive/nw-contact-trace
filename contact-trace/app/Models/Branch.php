<?php 

namespace ContactTraceApp\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branches';

  	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'name',
		'branch_province_id',
        'branch_city_id'
    ];

    public function province()
    {
        return $this->belongsTo(\ContactTraceApp\Models\Province::class, 'branch_province_id');
    }

    public function city()
    {
        return $this->belongsTo(\ContactTraceApp\Models\City::class, 'branch_city_id');
    }

    public function check_ins()
    {
        return $this->hasMany(\ContactTraceApp\Models\City::class, 'branch_id');
    }
}
