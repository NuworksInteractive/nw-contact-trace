<?php 

namespace ContactTraceApp\Models;

use Illuminate\Database\Eloquent\Model;

class CheckIn extends Model
{
    protected $table = 'check_in';

  	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'contact_id',
		'branch_id',
		'status'
    ];

    public function contact()
    {
        return $this->belongsTo(\ContactTraceApp\Models\Contact::class, 'contact_id');
    }

    public function branch()
    {
        return $this->belongsTo(\ContactTraceApp\Models\Branch::class, 'branch_id');
    }
}
