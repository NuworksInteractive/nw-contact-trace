<?php 

namespace ContactTraceApp\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

  	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'province_id',
		'city_name'
    ];

    public function province()
    {
        return $this->belongsTo(\ContactTraceApp\Models\Province::class, 'province_id', 'id');
    }
}
