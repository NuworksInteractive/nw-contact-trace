<?php 

namespace ContactTraceApp\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireAnswer extends Model
{
    protected $table = 'questionnaire_answers';

  	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'contact_id',
        'question',
		'answer'
    ];

    public function contact()
    {
        return $this->belongsTo(\ContactTraceApp\Models\Contact::class, 'contact_id');
    }
}
