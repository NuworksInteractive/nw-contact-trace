<?php

namespace ContactTraceApp;

use Illuminate\Foundation\Application;

class ContactTrace extends Application
{
    /**
     * The application namespace
     * 
     * @var string
     */
    protected $namespace = 'ContactTraceApp\\';
    /**
     * Get the path to the application "app" directory
     * 
     * @param string $path Optionally, a path to append to the path
     * 
     * @return string
     */
    public function path($path = '')
    {
        return $this->basePath . DIRECTORY_SEPARATOR . 'contact-trace' . DIRECTORY_SEPARATOR . 'app' . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}}
