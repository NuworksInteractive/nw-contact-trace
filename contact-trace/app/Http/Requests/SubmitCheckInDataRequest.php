<?php
 
namespace ContactTraceApp\Http\Requests;
 
use Illuminate\Foundation\Http\FormRequest;
 
class SubmitCheckInDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
     * Get the validation rules that apply to the request
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'                 => ['required', 'email'],
            'firstname'             => 'required',
            'lastname'              => 'required',
            'mobile_no'             => 'required',
            'current_address'       => 'required',
            'province_id'           => 'required',
            'city_id'               => 'required',
            'branch_id'             => 'required',
            'same_residence'        => 'required',
            'questionnaire_answers' => ['required', 'array'],
            'digest'                => 'required'
        ];
    }

  /**
   * Get the error messages for the defined validation rules
   *
   * @return array
   */
    public function messages()
    {
        return [
            'questionnaire_answers.array' => 'The questionnaire answers field is required',
            'same_residence'              => 'The questionnaire answers field is required'
        ];
    }
}