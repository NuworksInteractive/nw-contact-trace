<?php

namespace ContactTraceApp\Http\Controllers\Admin;

use Auth;
use Validator;
use Illuminate\Http\Request;

class LoginController extends \ContactTraceApp\Http\Controllers\Controller
{
    public function __construct()
    {
        // Nothing to see here...
    }

    public function getRedirectToLoginPage()
    {
        return redirect('admin/login');
    }

    public function getLoginPage()
    {
        if(auth()->check()) {
            return redirect()->route('admin.get.dashboard');
        }

        return view('admin.login');
    }

    public function postAuthenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => ['required', 'email'],
            'password' => ['required', 'min:15'],
        ]);

        if($validator->fails()) {
            return $this->returnToLoginPage();
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('admin.get.dashboard');
        }

        return $this->returnToLoginPage();
    }

    private function returnToLoginPage($with_error = true)
    {
        return redirect()->route($with_error == true ? 'admin.get.login.with-error' : 'admin.get.login');
    }

    public function getLogOut()
    {
        auth()->logout();

        return $this->returnToLoginPage($with_error = false);
    }
}