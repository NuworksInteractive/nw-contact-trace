<?php

namespace ContactTraceApp\Http\Controllers\Admin;

use Illuminate\Http\Request;
use ContactTraceApp\Models\City;
use ContactTraceApp\Models\Contact;
use ContactTraceApp\Models\Province;
use ContactTraceApp\Exports\CheckinReportExcel;

class DashboardController extends \App\Http\Controllers\Controller
{
    public function getDashboardPage()
    {
    	return view('admin.report');
    }

    public function postDeleteData(Request $request)
    {
        if( ! empty($request->id)) {
            Contact::whereId($request->id)->update([
                'deleted_at' => now()
            ]);
        }
    }

    public function getDataExport(Request $request)
    {
        $data = $this->getData($request);

        $date_range = explode(' - ', $request->date_range);

        $filename = date('Y_m_d') . '.csv';
        if(count($date_range) == 2) {
            $filename = date('Y_m_d', strtotime($date_range[0])) . ' - ' . date('Y_m_d', strtotime($date_range[1])) . '.csv';
        }

        return (new CheckinReportExcel($data['data']))->download($filename);
    }

    public function getData(Request $request)
    {
    	$records = Contact::whereNull('deleted_at')->with(['checkin', 'questionnaire_answers'])
    		->select('id', 'firstname', 'lastname', 'address_province_id', 'address_city_id', 'created_at');

        $date_range = explode(' - ', $request->date_range);

        if(count($date_range) == 2) {
            $records = $records->whereBetween('created_at', [
                date('Y-m-d 00:00:00', strtotime($date_range[0])), date('Y-m-d 23:59:59', strtotime($date_range[1]))
            ]);
        }

        $records = $records->whereHas('checkin', function($query) use($request) {
            if( ! empty($request->status)) {
                $query->whereStatus($request->status);
            }
        });

    	$total_records = $records->get();
    	$records = $records
    		->offset($request->start ?: 0)
    		->limit($request->length ?: 10)
    		->latest()
    		->get();

    	$records = $records->map(function($item) {
    		$city = City::find($item->address_city_id);
    		$province = Province::find($item->address_province_id);

    		return [
				date('Y-m-d h:i:s A', strtotime($item->created_at)),
				$item->firstname,
				$item->lastname,
				$item->checkin ? $item->checkin->status : null,
				$city ? $city->city_name : null,
				$province ? $province->province_name : null,
                '<button class="btn btn-sm btn-primary view-symptoms">Symptoms</button> &nbsp; <button class="btn btn-sm btn-danger" onClick="deleteMe('.$item->id.')">Delete</button>',
                isset($item->questionnaire_answers[0]) && $item->questionnaire_answers[0]->answer == '0' ? 'No' : 'Yes',
                isset($item->questionnaire_answers[1]) && $item->questionnaire_answers[1]->answer == '0' ? 'No' : 'Yes',
                isset($item->questionnaire_answers[2]) && $item->questionnaire_answers[2]->answer == '0' ? 'No' : 'Yes',
                isset($item->questionnaire_answers[3]) && $item->questionnaire_answers[3]->answer == '0' ? 'No' : 'Yes',
                isset($item->questionnaire_answers[4]) && $item->questionnaire_answers[4]->answer == '0' ? 'No' : 'Yes',
                isset($item->questionnaire_answers[5]) && $item->questionnaire_answers[5]->answer == '0' ? 'No' : 'Yes',
                isset($item->questionnaire_answers[6]) && $item->questionnaire_answers[6]->answer == '0' ? 'No' : 'Yes',
                isset($item->questionnaire_answers[7]) && $item->questionnaire_answers[7]->answer == '0' ? 'No' : 'Yes'
    		];
    	});

    	$response['data'] = $records->toArray();
		$response['draw'] = isset($request->draw) ? $request->draw : 1;
		$response['recordsTotal'] = count($records);
		$response['recordsFiltered'] = count($total_records);

    	return $response;
    }
}