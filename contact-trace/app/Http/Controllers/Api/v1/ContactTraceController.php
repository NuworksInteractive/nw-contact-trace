<?php

namespace ContactTraceApp\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use ContactTraceApp\Jobs\ContactSendToKafkaJob;
use ContactTraceApp\Repositories\ContactTraceRepository;
use ContactTraceApp\Http\Requests\SubmitCheckInDataRequest;

class ContactTraceController extends \ContactTraceApp\Http\Controllers\Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \ContactTraceApp\Repositories\ContactTraceRepository $repo
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, ContactTraceRepository $repo)
    {
        try {
            $data = app(SubmitCheckInDataRequest::class)->validated();

            $this->checkIfValidData($request);

            $user = $repo->saveContact($request);
            $check_in = $repo->saveCheckin($request, $user->id);

            $request->replace([
                'result'    => $check_in->status,
                'location'  => $check_in->branch_name,
                'timestamp' => date('d M Y h:i A', strtotime($check_in->created_at))
            ]);

            $data = [
                'location'    => $check_in->branch_name.' '.$check_in->city_name.', '.$check_in->province_name,
                'checkInTime' => date('Y-m-d H:i:s', strtotime($check_in->created_at)),
                'contactId'   => $user->contact_id_no,
                'status'      => $check_in->status
            ];

            //Job
            ContactSendToKafkaJob::dispatch($data);

            return response()->json($request->all(), HTTP_RESPONSE_SUCCESS);
        } catch(\Illuminate\Validation\ValidationException $ve) {
            return response()->json([ERROR_KEY => $ve->validator->errors()->all()], HTTP_RESPONSE_ENTITY_NOT_PROCESS);
        } catch(\Exception $e) {
            return response()->json([ERROR_KEY => $e->getMessage()], HTTP_RESPONSE_ENTITY_NOT_PROCESS);
        }
    }

    /**
     * Check if post data matched digest 
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return boolean
     */
    public function checkIfValidData($request)
    {
        $digest = $request->digest;
        $request->request->remove('digest');
        $request_post_data = sha1(json_encode($request->all()));
        
        if($request_post_data !== $digest){
            throw new \Exception('Invalid data. Please try again.');
        }
        
        return TRUE;
    }
}