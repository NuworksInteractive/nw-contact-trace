<?php

namespace ContactTraceApp\Http\Controllers\Api\v1\Location;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use ContactTraceApp\Repositories\ProvincesRepository;

class ProvincesController extends \ContactTraceApp\Http\Controllers\Controller
{
    /**
     * Get status
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request, ProvincesRepository $repository)
    {
        try {
            $list = $repository->get();
            $request->merge($list);
            return response()->json($request->all(), HTTP_RESPONSE_SUCCESS);
        } catch(\Exception $e){
            return response()->json([ERROR_KEY => $e->getMessage()], HTTP_RESPONSE_ENTITY_NOT_PROCESS);
        }  
    }
}