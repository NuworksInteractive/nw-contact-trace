<?php

namespace ContactTraceApp\Http\Controllers\Api\v1\Location;

use Illuminate\Http\Request;
use ContactTraceApp\Repositories\CitiesRepository;

class CitiesController extends \ContactTraceApp\Http\Controllers\Controller
{
    /**
     * Get status
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request, CitiesRepository $repository)
    {
        try {
            $province_id = $request->province_id ? $request->province_id : NULL;
            $data = $repository->get($province_id);
            return response()->json($data, HTTP_RESPONSE_SUCCESS);
        } catch(\Exception $e){
            return response()->json([ERROR_KEY => $e->getMessage()], HTTP_RESPONSE_ENTITY_NOT_PROCESS);
        }  
    }
}