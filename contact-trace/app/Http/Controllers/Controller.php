<?php

namespace ContactTraceApp\Http\Controllers;

use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $key_message, $message, $status)
    {
        return response()->json([$key_message => $message], $status)->header(JWT_TOKEN_NAME, $token);
    }

    /**
     * Validation form
     * 
     * @return mixed
     */
    protected function validation($request, $rules, $messages = [])
    {
        $validator = Validator::make($request, $rules, $messages);
        
        if($validator->fails()){
            $errors = [];
            return $errors = $validator->errors()->all();
        }
        return false;
    }
}
