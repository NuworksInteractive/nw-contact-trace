<?php

namespace ContactTraceApp\Http\Controllers;

use Illuminate\Http\Request;

class DefaultController extends \ContactTraceApp\Http\Controllers\Controller
{
    public function getPage()
    {
        return view('react.index');
    }
}