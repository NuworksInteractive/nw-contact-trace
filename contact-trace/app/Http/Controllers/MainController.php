<?php

namespace ContactTraceApp\Http\Controllers;

use Illuminate\Http\Request;
use ContactTraceApp\Http\Controllers\Controller;

class MainController extends Controller
{
    public function index()
    {
        return view('react.index');
    }
}

