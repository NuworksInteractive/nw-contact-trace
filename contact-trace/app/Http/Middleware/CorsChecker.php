<?php

namespace ContactTraceApp\Http\Middleware;

use Closure;

class CorsChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('app.env') != 'testing') {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
            header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization, registration-token, password-reset-token');
            header('Access-Control-Expose-Headers: access-token, registration-token, password-reset-token');
        }
    }
}
