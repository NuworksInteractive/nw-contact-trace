<?php

namespace ContactTraceApp\Repositories;

use DB;
use Illuminate\Http\Request; 
use ContactTraceApp\Models\Province;
use Illuminate\Support\Facades\Cache;

class ProvincesRepository extends \ContactTraceApp\Repositories\BaseRepository
{
    public function __construct(Province $model)
    {
        $this->model = $model;
    }

    /**
     * Get instance of ContactTraceApp\Models\Province
     *
     * @return ContactTraceApp\Models\Province
     */
    public function instance()
    {
        return $this->model;
    }

    /**
     * Get provinces
     *
     * @return ContactTraceApp\Models\Province
     */
    public function get()
    {            
        $key = 'cache.province';
        if(Cache::has($key)){
            $results = Cache::get($key);
        }else{
            $data = $this->model->orderBy('province_name', 'ASC')->get()->toArray();
            Cache::forever($key, $data);
            $results = Cache::get($key);
        }
        return $results;
    } 
}