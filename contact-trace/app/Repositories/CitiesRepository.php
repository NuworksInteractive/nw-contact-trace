<?php

namespace ContactTraceApp\Repositories;

use DB;
use Illuminate\Http\Request; 
use ContactTraceApp\Models\City;
use Illuminate\Support\Facades\Cache;

class CitiesRepository extends \ContactTraceApp\Repositories\BaseRepository
{
    public function __construct(City $model)
    {
        $this->model = $model;
    }

    /**
     * Get instance of ContactTraceApp\Models\City
     *
     * @return ContactTraceApp\Models\City
     */
    public function instance()
    {
        return $this->model;
    }

    /**
     * Get cities
     *
     * @param int province_id
     *
     * @return ContactTraceApp\Models\City
     */
    public function get(int $province_id = null)
    {
        $key = 'cache.city.province_'.$province_id;
        if(Cache::has($key)){
            $results = Cache::get($key);
        }else{
            $data = $this->model->whereProvinceId($province_id)->get()->toArray();
            Cache::forever($key, $data);
            $results = Cache::get($key);
        }
        return $results;
    } 
}