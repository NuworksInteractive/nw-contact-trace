<?php

namespace ContactTraceApp\Repositories;

use DB;
use Illuminate\Http\Request; 
use ContactTraceApp\Models\Branch;
use Illuminate\Support\Facades\Cache;

class BranchesRepository extends \ContactTraceApp\Repositories\BaseRepository
{
    public function __construct(Branch $model)
    {
        $this->model = $model;
    }

    /**
     * Get instance of ContactTraceApp\Models\Branch
     *
     * @return ContactTraceApp\Models\Branch
     */
    public function instance()
    {
        return $this->model;
    }

    /**
     * Get branches
     *
     * @return ContactTraceApp\Models\Branch
     */
    public function get()
    {
        $key = 'cache.branch';
        if(Cache::has($key)){
            $results = Cache::get($key);
        }else{
            $data = $this->model->select(['name', 'branch_province_id', 'branch_city_id'])->get()->toArray();
            Cache::forever($key, $data);
            $results = Cache::get($data);
        }
        return $results;
    } 
}
