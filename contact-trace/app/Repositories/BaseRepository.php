<?php

namespace ContactTraceApp\Repositories;

use ContactTraceApp\Repositories\RepositoryFactory;

abstract class BaseRepository implements RepositoryFactory
{
    /** 
     * The instance of current loaded model
     * 
     * @var mixed
     */
    public $model;

    /**
     * Location of our repository data
     *
     * @var array
     */
    protected $container = [];


    /**
     * Get instance of currently instantiated model
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function instance()
    {
        return $this->model;
    }

    /**
     * Return eloquent model
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function eloquent()
    {
        return $this->model;
    }

    /**
     * Find model by id
     *
     * @param integer $id
     * 
     * @return mixed
     */
    public function findById(int $id, array $with = [])
    {
        return $this->find($id, $with);
    }


    /**
     * Find user by id, included relations
     *
     * @param integer $id
     * 
     * @return mixed
     */
    public function find(int $id, array $with = [])
    {
        return $this->make($with)->find($id);
    }

    /**
     * Alias of make()
     *
     * @param array $with
     * 
     * @return mixed
     */
    public function with(array $with)
    {
        return $this->make($with);
    }

    /**
	 * Find an entity with it's relations
	 *
	 * @param array $with          The array query of relations
     * 
	 * @return Illuminate\Database\Eloquent
	 */
    public function make(array $with = [])
    {
        return $this->model->with($with);
    }
    
    public function first()
    {
        return $this->model->first();
    }

    public function all()
    {
        return $this->model->all();
    }

}