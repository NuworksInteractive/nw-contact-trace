<?php

namespace ContactTraceApp\Repositories;

interface RepositoryFactory
{
    public function find(int $id, array $with = []);

}