<?php

namespace ContactTraceApp\Repositories;

use ContactTraceApp\Models\Contact;
use ContactTraceApp\Models\CheckIn;
use ContactTraceApp\Models\Branch;
use Illuminate\Support\Facades\Cache;
use ContactTraceApp\Models\QuestionnaireAnswer;

class ContactTraceRepository extends \ContactTraceApp\Repositories\BaseRepository
{
    CONST NO = '0';
    CONST YES = '1';
    CONST EXPECTED_FAIL = 0;
    CONST RESULT_PASS = 'Passed';
    CONST RESULT_FAIL = 'Failed';
    CONST CACHE_CHECKIN_LOCATION = 'cache.checkin.location';

    /**
     * @var \ContactTraceApp\Models\Contact
     */
    protected $contact;

    /**
     * @var \ContactTraceApp\Models\CheckIn 
     */
    protected $checkIn;

    /**
     * @var \ContactTraceApp\Models\QuestionnaireAnswer 
     */
    protected $questionnaire_answer;

    /**
     * @param \ContactTraceApp\Models\Contact
     * @param \ContactTraceApp\Models\CheckIn
     * @param \ContactTraceApp\Models\QuestionnaireAnswer
     */
    public function __construct(
        Contact $contact,
        CheckIn $checkIn,
        QuestionnaireAnswer $questionnaire_answer
    )
    {
        $this->contact = $contact;
        $this->checkIn = $checkIn;
        $this->questionnaire_answer = $questionnaire_answer;
    }

    /**
     * Save contacts in db
     *
     * @param Object $request 
     *
     * @return \ContactTraceApp\Models\Contact
     */
    public function saveContact($request)
    {
        return \DB::transaction(function() use($request) {
            $user = $this->contact->create([
                'contact_id_no'       => uniqid().strtotime(now()),
                'firstname'           => $request->firstname,
                'lastname'            => $request->lastname,
                'email'               => $request->email,
                'mobile_no'           => $request->mobile_no,
                'current_address'     => $request->current_address,
                'address_province_id' => $request->province_id,
                'address_city_id'     => $request->city_id,
            ]);
            if($user->save()){
                return $user;
            }
            throw \Exception('Oops. something went wrong while saving. Please try again.');
        });
    }

    /**
     * Save checkins in db
     *
     * @param Object $request 
     * @param int $contact_id
     *
     * @return \ContactTraceApp\Models\Checkin
     */
    public function saveCheckin($request, $contact_id)
    {
        $status = $this->checkStatus($request->questionnaire_answers);
        $status = $request->same_residence == self::YES ? self::RESULT_FAIL : $status;
        return \DB::transaction(function() use($request, $contact_id, $status) {
            $checkIn = $this->checkIn->create([
                'contact_id' => $contact_id,
                'branch_id'  => $request->branch_id,
                'status'     => $status
            ]);
            if($checkIn->save()){
                $this->saveQuestionnaireAnswer($request, $contact_id);

                if(Cache::has(self::CACHE_CHECKIN_LOCATION)){
                    $location = Cache::get(self::CACHE_CHECKIN_LOCATION);
                }else{
                    $data = Branch::with(['city.province'])->get()->toArray();
                    $location = [];
                    foreach ($data as $key => $value) {
                        $location[] = [
                            'branch_id'     => $value['id'],
                            'branch_name'   => $value['name'],
                            'province_name' => $value['city']['province']['province_name'],
                            'city_name'     => $value['city']['city_name']
                        ];
                    }
                    Cache::forever(self::CACHE_CHECKIN_LOCATION, $location);
                    $location = Cache::get(self::CACHE_CHECKIN_LOCATION);
                }

                $key = array_search($request->branch_id, array_column($location, 'branch_id'));

                return (object) [
                    'status'        => $status,
                    'branch_name'   => $location[$key]['branch_name'],
                    'province_name' => $location[$key]['province_name'],
                    'city_name'     => $location[$key]['city_name'],
                    'created_at'    => $checkIn->created_at
                ];
            }
            throw \Exception('Oops. something went wrong while saving. Please try again.');
        });
    }

    /**
     * Save questions and answer from user in db
     *
     * @param Object $request 
     * @param int $contact_id
     *
     * @return \ContactTraceApp\Models\QuestionnaireAnswer
     */
    public function saveQuestionnaireAnswer($request, $contact_id)
    {
        return \DB::transaction(function() use($request, $contact_id) {
            if(!empty($request->questionnaire_answers)){    
                $data = [];
                foreach ($request->questionnaire_answers as $key => $value) {
                    $data[] = [
                        'contact_id' => $contact_id,
                        'question'   => $key,
                        'answer'     => $value,
                        'created_at' => now(),
                        'updated_at' => now()
                    ];

                    if($key === array_key_last($request->questionnaire_answers)) {
                        $data[] = [
                            'contact_id' => $contact_id,
                            'question'   => SAME_RESIDENCE_QUESTION_KEY,
                            'answer'     => $request->same_residence,
                            'created_at' => now(),
                            'updated_at' => now()
                        ];
                    }
                }

                return $this->questionnaire_answer->insert($data);
            }
            throw \Exception('Oops. something went wrong while saving. Please try again.');
        });
    }

    /**
     * Check if contact is pass or fail
     *
     * @param array $questionnaire_answers 
     *
     * @return boolean
     */
    private function checkStatus($questionnaire_answers)
    {
        $result = array_count_values($questionnaire_answers);
        if(isset($result[self::YES]) && $result[self::YES] > self::EXPECTED_FAIL){
            return self::RESULT_FAIL;
        }
        return self::RESULT_PASS;
    }
}