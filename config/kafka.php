<?php

return [

	'broker' => '192.168.1.10:9092',
	
	'topics' => [
		'check_in',
		'suspect',
		'self_isolate',
		'positive',
	],

];