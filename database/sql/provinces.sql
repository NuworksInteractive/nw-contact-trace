INSERT INTO `provinces` (`province_name`, `country_name`, `created_at`, `updated_at`) VALUES
('Abra', 'Philippines', NULL, NULL),
('Agusan del Norte', 'Philippines', NULL, NULL),
('Agusan del Sur', 'Philippines', NULL, NULL),
('Aklan', 'Philippines', NULL, NULL),
('Albay', 'Philippines', NULL, NULL),
('Antique', 'Philippines', NULL, NULL),
('Apayao', 'Philippines', NULL, NULL),
('Aurora', 'Philippines', NULL, NULL),
('Basilan', 'Philippines', NULL, NULL),
('Bataan', 'Philippines', NULL, NULL),
('Batanes', 'Philippines', NULL, NULL),
('Batangas', 'Philippines', NULL, NULL),
('Benguet', 'Philippines', NULL, NULL),
('Biliran', 'Philippines', NULL, NULL),
('Bohol', 'Philippines', NULL, NULL),
('Bukidnon', 'Philippines', NULL, NULL),
('Bulacan', 'Philippines', NULL, NULL),
('Cagayan', 'Philippines', NULL, NULL),
('Camarines Norte', 'Philippines', NULL, NULL),
('Camarines Sur', 'Philippines', NULL, NULL),
('Camiguin', 'Philippines', NULL, NULL),
('Capiz', 'Philippines', NULL, NULL),
('Catanduanes', 'Philippines', NULL, NULL),
('Cavite', 'Philippines', NULL, NULL),
('Cebu', 'Philippines', NULL, NULL),
('Compostela Valley', 'Philippines', NULL, NULL),
('Cotabato', 'Philippines', NULL, NULL),
('Davao del Norte', 'Philippines', NULL, NULL),
('Davao del Sur', 'Philippines', NULL, NULL),
('Davao Oriental', 'Philippines', NULL, NULL),
('Eastern Samar', 'Philippines', NULL, NULL),
('Guimaras', 'Philippines', NULL, NULL),
('Ifugao', 'Philippines', NULL, NULL),
('Ilocos Norte', 'Philippines', NULL, NULL),
('Ilocos Sur', 'Philippines', NULL, NULL),
('Iloilo', 'Philippines', NULL, NULL),
('Isabela', 'Philippines', NULL, NULL),
('Kalinga', 'Philippines', NULL, NULL),
('La Union', 'Philippines', NULL, NULL),
('Laguna', 'Philippines', NULL, NULL),
('Lanao del Norte', 'Philippines', NULL, NULL),
('Lanao del Sur', 'Philippines', NULL, NULL),
('Leyte', 'Philippines', NULL, NULL),
('Maguindanao', 'Philippines', NULL, NULL),
('Marinduque', 'Philippines', NULL, NULL),
('Masbate', 'Philippines', NULL, NULL),
('Metro Manila', 'Philippines', NULL, NULL),
('Misamis Occidental', 'Philippines', NULL, NULL),
('Misamis Oriental', 'Philippines', NULL, NULL),
('Mountain Province', 'Philippines', NULL, NULL),
('Negros Occidental', 'Philippines', NULL, NULL),
('Negros Oriental', 'Philippines', NULL, NULL),
('Northern Samar', 'Philippines', NULL, NULL),
('Nueva Ecija', 'Philippines', NULL, NULL),
('Nueva Vizcaya', 'Philippines', NULL, NULL),
('Occidental Mindoro', 'Philippines', NULL, NULL),
('Oriental Mindoro', 'Philippines', NULL, NULL),
('Palawan', 'Philippines', NULL, NULL),
('Pampanga', 'Philippines', NULL, NULL),
('Pangasinan', 'Philippines', NULL, NULL),
('Quezon', 'Philippines', NULL, NULL),
('Quirino', 'Philippines', NULL, NULL),
('Rizal', 'Philippines', NULL, NULL),
('Romblon', 'Philippines', NULL, NULL),
('Samar', 'Philippines', NULL, NULL),
('Sarangani', 'Philippines', NULL, NULL),
('Siquijor', 'Philippines', NULL, NULL),
('Sorsogon', 'Philippines', NULL, NULL),
('South Cotabato', 'Philippines', NULL, NULL),
('Southern Leyte', 'Philippines', NULL, NULL),
('Sultan Kudarat', 'Philippines', NULL, NULL),
('Sulu', 'Philippines', NULL, NULL),
('Surigao del Norte', 'Philippines', NULL, NULL),
('Surigao del Sur', 'Philippines', NULL, NULL),
('Tarlac', 'Philippines', NULL, NULL),
('Tawi-Tawi', 'Philippines', NULL, NULL),
('Zambales', 'Philippines', NULL, NULL),
('Zamboanga del Norte', 'Philippines', NULL, NULL),
('Zamboanga del Sur', 'Philippines', NULL, NULL),
('Zamboanga Sibugay', 'Philippines', NULL, NULL);