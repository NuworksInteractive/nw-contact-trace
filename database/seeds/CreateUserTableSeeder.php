<?php

use App\User;
use Illuminate\Database\Seeder;

class CreateUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name'     => 'Administrator',
                'email'    => 'admin@nuworks.ph',
                'password' => '(s[F[5a}[S7tFC~'
            ]
        ];

        foreach($users as $user) {
            $user = User::firstOrNew($user);
            $user->save();
        }
    }
}
