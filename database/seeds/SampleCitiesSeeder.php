<?php

use Illuminate\Database\Seeder;
use ContactTraceApp\Models\City;

class SampleCitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'city_name' => 'Manila',
                'province_id' => 1
            ],
            [
                'city_name' => 'Quezon City',
                'province_id' => 1
            ],
        ];

        foreach($cities as $city) {
            $city = City::firstOrNew($city);
            $city->save();
        }
    }
}
