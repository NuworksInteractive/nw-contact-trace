<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CreateUserTableSeeder::class);
        
        if (config('app.env') != 'testing') {
            $this->call(CreateProvincesTableSeeder::class);
            $this->call(CreateCitiesTableSeeder::class);
            $this->call(CreateBranchesTableSeeder::class);
        }
    }
}
