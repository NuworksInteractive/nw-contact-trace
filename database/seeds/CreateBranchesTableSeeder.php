<?php

use Illuminate\Database\Seeder;
use ContactTraceApp\Models\Branch;

class CreateBranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branches = [
            [
                'name' => 'Jollibee Tektite',
                'branch_province_id' => 47,
                'branch_city_id' => 975
            ],
            [
                'name' => 'Jollibee Emerald Avenue',
                'branch_province_id' => 47,
                'branch_city_id' => 975
            ],
            [
                'name' => 'Jollibee SM Megamall B',
                'branch_province_id' => 47,
                'branch_city_id' => 968
            ],
        ];

        foreach($branches as $branch) {
            $branch = Branch::firstOrNew($branch);
            $branch->save();
        }
    }
}
