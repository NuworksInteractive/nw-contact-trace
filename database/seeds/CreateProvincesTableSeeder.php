<?php

use Illuminate\Database\Seeder;

class CreateProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::unprepared('SET FOREIGN_KEY_CHECKS=0;');
        \DB::unprepared(file_get_contents(__DIR__.'/../sql/provinces.sql'));
        \DB::unprepared('SET FOREIGN_KEY_CHECKS=1;');
    }
}
