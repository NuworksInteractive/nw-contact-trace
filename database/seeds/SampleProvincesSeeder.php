<?php

use Illuminate\Database\Seeder;
use ContactTraceApp\Models\Province;

class SampleProvincesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces = [
            [
                'province_name' => 'NCR',
                'country_name' => 'Philippines'
            ],
        ];

        foreach($provinces as $province) {
            $province = Province::firstOrNew($province);
            $province->save();
        }
    }
}
