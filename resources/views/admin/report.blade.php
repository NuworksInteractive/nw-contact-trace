<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Contact Tracing</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="{{ asset('backend/plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/plugins/daterangepicker/daterangepicker.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style>
    .bootstrap-datetimepicker-widget tr:hover {
      background-color: #808080;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="../../index3.html" class="brand-link">
      <span class="brand-text font-weight-light">Contact Tracing</span>
    </a>

    <div class="sidebar">
      
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="nav-icon fa fa-table"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>CHECK IN REPORT</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.get.logout') }}" class=" btn btn-primary">Logout</a></li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <center>
                  <form>
                  <div class="col-md-7">
                      <div class="box box-solid box-default">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="far fa-calendar-alt"></i>
                              </span>
                            </div>
                            <input type="text" class="form-control float-right" id="date-ranger" name="date_range" value="{{ request()->date_range ?: null }}">
                            &nbsp;
                            <select class="form-control" name="status" width="100px">
                              <option value="">Status</option>
                              <option value="Passed" {{ request()->status == 'Passed' ? 'selected="selected"' : null }}>Passed</option>
                              <option value="Failed" {{ request()->status == 'Failed' ? 'selected="selected"' : null }}>Failed</option>
                            </select>
                            &nbsp;
                            <button class="btn btn-primary">Apply</button>
                            &nbsp;
                            <a href="#" class="btn btn-primary export">Export</a>
                          </div>
                      </div><!-- /.box -->
                  </div>
                  </form>
                  <br>
                </center>
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>
                      Timestamp
                    </th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Status</th>
                    <th>City</th>
                    <th>Province</th>
                    <th style="width: 150px">Action</th>
                  </tr>
                  </thead>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
  </footer>


  <aside class="control-sidebar control-sidebar-dark">
  </aside>

  <div id="myModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Symptomps Information</h5>
        </div>
        <div class="modal-body">
          <table>
            <tr>
              <td>Colds:</td>
              <td><span class="colds"></span></td>
            </tr>
            <tr>
              <td>Cough:</td>
              <td><span class="cough"></span></td>
            </tr>
            <tr>
              <td>Sore Throat:</td>
              <td><span class="sore-throat"></span></td>
            </tr>
            <tr>
              <td>Diarrhea:</td>
              <td><span class="diarrhea"></span></td>
            </tr>
            <tr>
              <td>Fever:</td>
              <td><span class="fever"></span></td>
            </tr>
            <tr>
              <td>Shortness of Breath:</td>
              <td><span class="shortness-of-breath"></span></td>
            </tr>
            <tr>
              <td>Travelled:</td>
              <td><span class="travelled"></span></td>
            </tr>
            <tr>
              <td>Same residence with confirmed COVID-19 case:</td>
              <td><span class="same-residence"></span></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

</div>

<script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('backend/dist/js/demo.js') }}"></script>
<script src="{{ asset('backend/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('backend/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  var table;
  $(function () {
    table = $('#example2').DataTable({
      processing: true,
      serverSide: true,
      ordering: false,
      searching: false,
      response: true,
      ajax: "{{ route('admin.get.dashboard.data') }}?date_range={{ request()->date_range }}&status={{ request()->status }}"
    });

    $('#date-ranger').daterangepicker();

    $('#example2 tbody').on('click', '.view-symptoms', function () {        
        var data = table.row( $(this).parents('tr') ).data();

        $(".same-residence").html(data[14]);
        $(".colds").html(data[7]);
        $(".cough").html(data[8]);
        $(".sore-throat").html(data[9]);
        $(".diarrhea").html(data[10]);
        $(".fever").html(data[11]);
        $(".shortness-of-breath").html(data[12]);
        $(".travelled").html(data[13]);

        $('#myModal').modal('show')
    });

    $('.export').on('click', function() {
      window.location.href = "{{ route('admin.get.dashboard.data.export') }}?date_range={{ request()->date_range }}&status={{ request()->status }}";
    });

    
  });

  function deleteMe(id) {
    swal({
      text: "Are you sure you want to delete this record?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.post("{{ route('admin.post.dashboard.delete') }}", {
          'id' : id,
          '_token': "{{ csrf_token() }}"
        }, function() {
          table.ajax.reload( null, false );
          swal("Poof! Your imaginary file has been deleted!", {
            icon: "success",
          });
        });
      }
    });
  }
</script>
</body>
</html>
