import React from 'react';
import logo from './logo.svg';
import 'antd/dist/antd.css';
import 'antd/dist/antd.js';
import './App.css';
import FormTPL from './components/Form/Form';

function App() {
  return (
    <div className='App'>
      <FormTPL></FormTPL>
    </div>
  );
}

export default App;
