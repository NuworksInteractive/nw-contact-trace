import React, { Component } from "react";
import {
    DatePicker,
    Space,
    Form,
    Input,
    Button,
    Checkbox,
    Select,
    Radio,
} from "antd";
import {
    MailOutlined,
    ToolFilled,
    MobileOutlined,
    HomeFilled,
    UserOutlined,
} from "@ant-design/icons";
import sha1 from "sha1";
import axios from "axios";
import { Row, Col } from 'antd';

const { Option } = Select;

class FormTPL extends Component {
    state = {
        visible: true,
        check1: 0,
        check2: 0,
        check3: 0,
        check4: 0,
        check5: 0,
        check6: 0,
        check7: 0,
        same_residence: 0,
        popup: false,
        success: false,
        timestamp: "",
        provinces: [],
        cities: [],
        branches: [],
        selectedCity: null,
    };

    componentDidMount() {
        axios
            .get("https://contact-tracing-qa.nuworks.ph/api/v1/provinces")
            .then(
                (response) => {
                    this.setState({ provinces: response.data });
                },
                (error) => {
                    console.log(error);
                }
            );

        axios.get("https://contact-tracing-qa.nuworks.ph/api/v1/branches").then(
            (response) => {
                this.setState({ branches: response.data });
                console.log(response.data);
            },
            (error) => {
                console.log(error);
            }
        );
    }

    onFinish = (values) => {
        let answers = [
            this.state.check1,
            this.state.check2,
            this.state.check3,
            this.state.check4,
            this.state.check5,
            this.state.check6,
            this.state.check7,
        ];

        let same_residence = this.state.same_residence;

        let encrypt = {
            email: values.email,
            firstname: values.firstname,
            lastname: values.lastname,
            mobile_no: values.mobile_no,
            current_address: values.current_address,
            province_id: values.province_id,
            city_id: values.city_id,
            branch_id: 1,
            same_residence: values.same_residence,
            questionnaire_answers: answers,
        };

        let string = JSON.stringify(encrypt);

        let encyrpted_string = sha1(string);

        console.log("Success:", string);

        let submitVals = {
            email: values.email,
            firstname: values.firstname,
            lastname: values.lastname,
            mobile_no: values.mobile_no,
            current_address: values.current_address,
            province_id: values.province_id,
            city_id: values.city_id,
            branch_id: 1,
            same_residence: values.same_residence,
            questionnaire_answers: answers,
            digest: encyrpted_string,
        };

        console.log(submitVals);

        axios
            .post(
                "https://contact-tracing-qa.nuworks.ph/api/v1/check-in",
                submitVals
            )
            .then(
                (response) => {
                    console.log(response.data.result);
                    if (response.data.result === "Failed") {
                        this.setState({
                            visible: false,
                            popup: true,
                            success: false,
                        });
                    } else {
                        this.setState({
                            visible: false,
                            popup: true,
                            success: true,
                            timestamp: response.data.timestamp,
                        });
                    }
                },
                (error) => {
                    console.log(error);
                    this.setState({
                        visible: false,
                        popup: true,
                        success: false,
                    });
                }
            );
    };

    onChange = (e) => {
        var stateObject = function () {
            let returnObj = {};
            let checked = this.target.checked ? 1 : 0;
            let value = this.target.value;
            // returnObj = { value: checked };
            returnObj[value] = checked;
            return returnObj;
        }.bind(e)();

        console.log(stateObject);
        this.setState(stateObject);
    };

    handleProvinceChange = (e) => {
        console.log(e);
        this.setState({
            selectedCity: 0,
        });
        axios
            .get("https://contact-tracing-qa.nuworks.ph/api/v1/cities", {
                params: { province_id: e },
            })
            .then(
                (response) => {
                    let dataArray = Object.keys(response.data).map(
                        (i) => response.data[i]
                    );
                    this.setState({ cities: dataArray });
                },
                (error) => {
                    console.log(error);
                }
            );
    };

    handleCityChange = (e) => {
        console.log(e);
        this.setState({
            selectedCity: e,
        });
    };

    onRadioChange = (e) => {
        var stateObject = function () {
            let returnObj = {};
            let value = this.target.value;
            returnObj["same_residence"] = value;
            return returnObj;
        }.bind(e)();

        console.log(stateObject);
    };

    onChangeDate = (e) => {};

    render() {
        const { cities, provinces, selectedCity } = this.state;

        let classes = this.state.visible ? "form-cont" : "form-cont hidden";
        let popupClasses = this.state.popup
            ? "popup-cont"
            : "popup-cont hidden";

        return (
            <div className="form-container">
                <div className='bar-cont'>
                <img src={process.env.PUBLIC_URL + '/image 1.png'} className='bar' /> 
                </div>
                
                <div className="header">Contact Tracing</div>
                <div className={classes}>

                    <Form onFinish={this.onFinish}>
                    <Row gutter={[16, 16]}>
                        <Col xs={24} md={12}>
                        <Form.Item
                            name="firstname"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your First Name!",
                                },
                            ]}
                        >
                            <Input
                                placeholder="First Name"
                                size="large"
                                className="form-input"
                                prefix={
                                    <UserOutlined className="site-form-item-icon" />
                                }
                            />
                        </Form.Item>
                        </Col>
                        <Col xs={24} md={12}>
                        <Form.Item
                            name="lastname"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your First Name!",
                                },
                            ]}
                        >
                            <Input
                                placeholder="Last Name"
                                size="large"
                                className="form-input"
                                prefix={
                                    <UserOutlined className="site-form-item-icon" />
                                }
                            />
                        </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={[16, 16]}>
                        <Col xs={24} md={12}>
                        <Form.Item
                            name="mobile_no"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your Mobile Number!",
                                },
                            ]}
                            
                        >
                          
                            <Input
                            
                                placeholder="Mobile Number"
                                size="large"
                                className="form-input"
                                prefix={
                                    <MobileOutlined className="site-form-item-icon" />
                                }
                                
                            />
                        </Form.Item>
                        </Col>
                        <Col xs={24} md={12}> 
                        <Form.Item
                            name="email"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your email address!",
                                },
                            ]}
                        >
                            <Input 
                                placeholder="Email Address"
                                size="large"
                                className="form-input"
                                prefix={
                                    <MailOutlined  className="site-form-item-icon" />
                                }
                            />
                        </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                        <Form.Item
                            name="current_address"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your Residence!",
                                },
                            ]}
                        >
                            <Input
                                placeholder="Residence"
                                size="large"
                                className="form-input"
                                prefix={
                                    <HomeFilled className="site-form-item-icon" />
                                }
                            />
                        </Form.Item>
                        </Col>
                    </Row>
                        <Form.Item
                            name="province_id"
                            rules={[
                                {
                                    required: true,
                                    message: "Please select a province !",
                                },
                            ]}
                        >
                            <Select
                                placeholder="Province"
                                onChange={this.handleProvinceChange}
                            >
                                {provinces.map((province) => (
                                    <Option
                                        key={province.id}
                                        value={province.id}
                                    >
                                        {province.province_name}
                                    </Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item
                            name="city_id"
                            rules={[
                                {
                                    required: true,
                                    message: "Please select a city !",
                                },
                            ]}
                        >
                            <Select
                                placeholder="City"
                                value={selectedCity}
                                onChange={this.handleCityChange}
                            >
                                {cities.map((city) => (
                                    <Option key={city.id} value={city.id}>
                                        {city.city_name}
                                    </Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Row>
                            <Col xs={24} md={12}>
                                <Form.Item name="branch_id">
                                    <Input
                                        placeholder="Nuworks Office"
                                        value="1"
                                        size="large"
                                        disabled="false"
                                        className="form-input"
                                        prefix={
                                            <svg width="18" height="24" viewBox="0 0 18 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M8.99995 0C7.83805 0.00164808 6.68785 0.232134 5.61503 0.678297C4.5422 1.12446 3.56776 1.77756 2.74734 2.60032C1.92692 3.42307 1.27658 4.39936 0.833468 5.47345C0.390352 6.54753 0.163133 7.69838 0.164784 8.86028C0.158474 10.1055 0.421973 11.3373 0.937154 12.471C3.14752 17.3061 7.37985 22.4113 8.62946 23.8556C8.71441 23.9538 8.8349 24.0143 8.96443 24.0237C9.09396 24.0331 9.22193 23.9907 9.3202 23.9058L9.37043 23.8556C10.6263 22.405 14.8524 17.3061 17.0627 12.471C17.5781 11.3374 17.8416 10.1055 17.8351 8.86028C17.8368 7.69838 17.6095 6.54753 17.1664 5.47345C16.7233 4.39936 16.073 3.42307 15.2526 2.60032C14.4321 1.77756 13.4577 1.12446 12.3849 0.678297C11.312 0.232134 10.1618 0.00164808 8.99995 0ZM8.99995 13.4192C8.09075 13.4191 7.20203 13.1491 6.4465 12.6433C5.69097 12.1375 5.10266 11.4188 4.75617 10.5782C4.40969 9.73758 4.32064 8.81303 4.50031 7.92176C4.67999 7.03049 5.1203 6.21265 5.7654 5.57196C6.41051 4.93126 7.23135 4.49658 8.12383 4.32302C9.01632 4.14947 9.94024 4.24486 10.7784 4.59711C11.6166 4.94936 12.3313 5.54258 12.8319 6.30157C13.3325 7.06055 13.5964 7.9511 13.5902 8.86028C13.5803 10.0717 13.0925 11.2303 12.2329 12.084C11.3733 12.9377 10.2114 13.4175 8.99995 13.4192Z" fill="#717171"/>
                                            </svg>                                            
                                        }
                                    />
                                </Form.Item>
                            </Col>
                        </Row>

                        <Form.Item
                            name="same_residence"
                            label="Have you stayed in the same residence as someone confirmed to have COVID-19?"
                            labelCol={{ span: 24 }}
                            className="health-label"
                            rules={[
                                {
                                    required: true,
                                    message: "Please answer YES or NO!",
                                },
                            ]}
                        >
                            <Radio.Group
                                onChange={this.onRadioChange}
                                value={this.state.value}
                            >
                                <Radio className="item" value={1}>
                                    Yes
                                </Radio>
                                <Radio className="item" value={0}>
                                    No
                                </Radio>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item
                            name="questionnaire_answers"
                            label="If you have been feeling any of the following symptoms, please select them below::"
                            labelCol={{ span: 24 }}
                            className="health-label"
                        >
                            <Checkbox.Group className="health-check">
                                <Checkbox
                                    value="check1"
                                    className="item"
                                    onChange={this.onChange}
                                >
                                    Colds
                                </Checkbox>
                                <Checkbox
                                    value="check2"
                                    className="item"
                                    onChange={this.onChange}
                                >
                                    Cough
                                </Checkbox>
                                <Checkbox
                                    value="check3"
                                    className="item"
                                    onChange={this.onChange}
                                >
                                    Sore Throat
                                </Checkbox>
                                <Checkbox
                                    value="check4"
                                    className="item"
                                    onChange={this.onChange}
                                >
                                    Diarrhea
                                </Checkbox>
                                <Checkbox
                                    value="check5"
                                    className="item"
                                    onChange={this.onChange}
                                >
                                    Fever
                                </Checkbox>
                                <Checkbox
                                    value="check6"
                                    className="item"
                                    onChange={this.onChange}
                                >
                                    Shortness of Breath
                                </Checkbox>
                                <Checkbox
                                    value="check7"
                                    className="item"
                                    onChange={this.onChange}
                                >
                                   Travelled
                                </Checkbox>
                             
                            </Checkbox.Group>
                        </Form.Item>
                        <Form.Item className='submit-cont'>
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="submit-btn"
                            >
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
                <div className={popupClasses}>
                    {this.state.success ? (
                        <div className="popup-message -success">
                            <h1 className="title">SUCCESS</h1>
                            <p>
                                You have succesfully checked in on{" "}
                                {this.state.timestamp}.{" "}
                            </p>
                        </div>
                    ) : (
                        <div className="popup-message -fail">
                            <h1 className="title -fail">FAILED</h1>
                            <p>
                                We apologize you cannot enter the establishment.
                            </p>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default FormTPL;
