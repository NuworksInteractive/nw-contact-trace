<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class AdminChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Str::contains($request->fullUrl(), 'admin')) {
            return $next($request);
        }

        if ( ! $request->session()->has('_admin')) {
            return redirect()->route('admin.get.login');
        }

        return $next($request);
    }
}
