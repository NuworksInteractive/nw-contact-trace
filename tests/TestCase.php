<?php

namespace Tests;

use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $app;

    public function setUp():void
    {
        parent::setUp();
        // Disable throttling during tests
        $this->withoutMiddleware(
            ThrottleRequests::class
        );

        // Set environment to testing
        config(['app.env' => 'testing']);
    }
}
