<?php

namespace Tests\Feature\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CitiesControllerTest extends \Tests\TestCase
{
    use RefreshDatabase;

    public function setUp():void
    {
        parent::setUp();
        $this->request = new Request;

        $this->seed('SampleProvincesSeeder');
        $this->seed('SampleCitiesSeeder');
        $this->seed('SampleBranchesSeeder');
    }

    /**
     * @test
     * @group feature_positive
     */
    public function it_should_get_cities_list()
    {
        $response = $this->json('GET', '/api/v1/cities', ['province_id' => '1']);
        $response
            ->assertStatus(HTTP_RESPONSE_SUCCESS)
            ->assertExactJson($response->original);
    }
}