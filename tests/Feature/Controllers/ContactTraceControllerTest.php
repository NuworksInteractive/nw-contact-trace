<?php

namespace Tests\Feature\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactTraceControllerTest extends \Tests\TestCase
{
    // use RefreshDatabase;

    public function setUp():void
    {
        parent::setUp();
        $this->request = new Request;

        $this->seed('SampleProvincesSeeder');
        $this->seed('SampleCitiesSeeder');
        $this->seed('SampleBranchesSeeder');
    }

    /**
     * @test
     * @group feature_positive
     */
    public function it_should_save_check_in_and_submit_if_pass_or_fail()
    {
        $this->request->replace([
            'fullname'  => 'John Doe',
            'mobile_no'  => '09777777777',
            'current_address' => 'QC',
            'province_id' => 1,
            'city_id' => 1,
            'branch_id' => 1,
            'questionnaire_answers' => [
                0,
                0,
                0
            ]
        ]);

        $convert_array_to_string = json_encode($this->request->all());
        $digest = sha1($convert_array_to_string);
        $this->request->merge([
            'digest' => $digest
        ]);

        $response = $this->json('POST', '/api/v1/check-in', $this->request->all());
        $response
            ->assertStatus(HTTP_RESPONSE_SUCCESS)
            ->assertJsonStructure([
                'result',
                'location',
                'timestamp'
            ]);
    }

    /**
     * @test
     * @group feature_negative
     */
    public function it_should_throw_error_response_if_post_data_doesnt_match_the_digest()
    {
        $this->request->replace([
            'fullname'  => 'John Doe',
            'mobile_no'  => '09777777777',
            'current_address' => 'QC',
            'province_id' => 1,
            'city_id' => 1,
            'branch_id' => 1,
            'questionnaire_answers' => [
                'no',
                'no',
                'yes'
            ]
        ]);

        $digest = sha1(uniqid());

        $this->request->merge([
            'digest' => $digest
        ]);

        $response = $this->json('POST', '/api/v1/check-in', $this->request->all());
        $response
            ->assertStatus(HTTP_RESPONSE_ENTITY_NOT_PROCESS)
            ->assertJsonFragment([
            'error' => 'Invalid data. Please try again.'
        ]);
    }
}