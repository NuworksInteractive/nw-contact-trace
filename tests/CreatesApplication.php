<?php

namespace Tests;

use Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        $this->useSqlite($app);
        // $env = new \Dotenv\Dotenv(__DIR__.'/../');
        // $env->load();

        $this->app = $app;

        return $app;
    }

    /**
     * Set database to sqlite during unit test
     *
     * @param ContactTraceApp\ContactTraceApp $app
     * 
     * @return void
     */
    private function useSqlite($app)
    {
        $db_driver = getenv('DB_DRIVER') ? getenv('DB_DRIVER') : 'sqlite_testing';
        $app['config']->set('database.default', $db_driver);
        Artisan::call('migrate:refresh');
        Artisan::call('migrate');
        Artisan::call("db:seed");
        Hash::setRounds(4);
    }

}
